#ifndef _LIB_CAN_H_
#define _LIB_CAN_H_

struct can_device
{
	char *if_name;
	unsigned char dev_num;
};

#endif
