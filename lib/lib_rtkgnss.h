#ifndef _LIB_RTKGNSS_H_
#define _LIB_RTKGNSS_H_

struct rtkgnss_device
{
	char *if_name;
	char *dev;
	unsigned int tcflags;
	unsigned int baud_rate;
};

#endif
