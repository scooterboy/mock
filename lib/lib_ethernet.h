#ifndef _LIB_ETHERNET_H_
#define _LIB_ETHERNET_H_

struct eth_device
{
	char *if_name;
	int gbit_flag;		/* 1 for Gbit port, 0 for 100 Mb port */
};

#endif
