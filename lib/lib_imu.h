#ifndef _LIB_IMU_H_
#define _LIB_IMU_H_

struct imu_device
{
	char *if_name;
	unsigned char i2c_address;
};

#endif
