#ifndef _LIB_ETHSWITCH_H_
#define _LIB_ETHSWITCH_H_

#define ETH_SWITCH_NUM_PORTS 12

struct switch_port
{
	const char *name;
	const char *port_test;	/* Command for testing this port */
	int gbit_flag;		/* 1 for Gbit port, 0 for 100 Mb port */
	int connected_flag;	/* 1 for connected port, 0 for disconnected */
};

struct eth_switch
{
	char *label;
	char *ip_address;
	char *user;
	char *manufacturer_string;
	struct switch_port ports[ETH_SWITCH_NUM_PORTS];
};

#endif
