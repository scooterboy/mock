COMPONENT ?= acu1
DEBUG ?= 0

CC := clang

$(shell rm -f components/current)
$(shell ln -s $(COMPONENT) components/current)

COMMON_INC = ../../inc

ifeq ($(VERBOSE), 1)
  Q =
else
  Q = @
endif

include components/current/cmd.cfg
include test/rules.mk

SRCS-y += $(wildcard components/current/*.c)
SRCS-y += $(filter-out version.c, $(wildcard *.c))
SRCS-y += version.c
SRCS-y += $(wildcard lib/*.c)
HEADERS += $(wildcard components/current/*.h)
HEADERS += $(wildcard *.h)
HEADERS += $(wildcard $(COMMON_INC)/*.h)
HEADERS += $(EXTRA_HDR)
OBJS = $(SRCS-y:.c=.o)

INCLUDES += -I ./
INCLUDES += -I ./lib
INCLUDES += -I $(COMMON_INC)

CFLAGS  += -Wall -Werror
CFLAGS  += -DDEBUG=$(DEBUG)
LDFLAGS += -lreadline

.PHONY: all clean pre_build

all: | pre_build bist

-include components/current/extra.cfg

# Put together BIST binary
bist: $(OBJS)
	@echo "Linking $@"
	$(Q)$(CC) -o $@ $(OBJS) $(LDFLAGS)

# Compile C files
%.o: %.c $(HEADERS)
	@echo "Compiling $@"
	$(Q)$(CC) -c -O2 -o $@ $< $(CFLAGS) $(INCLUDES)

test/%.o: test/%.c $(HEADERS)
	@echo "Compiling $@"
	$(Q)$(CC) -c -O2 -o $@ $< $(CFLAGS) $(INCLUDES)

version.o: version.c $(HEADERS)
	@echo "Compiling $@"
	$(Q)$(CC) -c -O2 -o $@ $< $(CFLAGS) $(INCLUDES)

# Generate version.c file
pre_build:
	$(shell ./version.sh > version.c)
	@echo ">> version.c generated"

clean:
	@rm -f bist
	@rm -f version.c
	@find . -type f -name '*.o' -delete
