#ifndef __BIST_H__
#define __BIST_H__

#include <stdio.h>
#include "components/component_config.h"
#define PROTOTYPES
#include "applets.h"
#undef PROTOTYPES

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
#endif

#define PROMPT			"BIST>"

#define BIST_MENU		"97"
#define BIST_HELP		"98"
#define BIST_EXIT		"99"

enum test_status
{
	TEST_PASSED,
	TEST_FAILED,
	TEST_RUNNING,
	TEST_ONGOING,
};

const struct component_config *component_config_get(void);
int bist_getc(void);
int bist_printf(const char *fmt, ...) __attribute__((format(printf, 1, 2)));
int bist_errmsg(const char *fmt, ...) __attribute__((format(printf, 1, 2)));
int bist_exec(const char *fmt, ...) __attribute__((format(printf, 1, 2)));
int bist_print_buffer(unsigned long addr, void *data, unsigned int width, unsigned int count);

#define bist_dbg(fmt, ...) \
    do { if (DEBUG) fprintf(stdout, fmt, ##__VA_ARGS__); } while (0)

#endif /* __BIST_H__ */
