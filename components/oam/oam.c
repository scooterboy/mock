#include "bist.h"
#include "lib_ethernet.h"
#include "lib_can.h"

static int oam_init(void)
{
	return 0;
}

static const struct eth_device eth_devices[] =
{
	{
		.if_name = "eth1",
		.gbit_flag = 1,
	}
};

static const struct can_device can_devices[] =
{
	{
		.if_name = "can0",
	},
};

static const struct component_config oam_config =
{
	.component_name = "oam",
	.component_init = &oam_init,

	.eth_devices = eth_devices,
	.eth_devices_cnt = ARRAY_SIZE(eth_devices),

	.can_devices = can_devices,
	.can_devices_cnt = ARRAY_SIZE(can_devices),
};

const struct component_config* component_config_get(void)
{
	return &oam_config;
}
