#include "bist.h"
#include "lib_ethernet.h"
#include "lib_can.h"
#include "imu.h"

static int acu2_init(void)
{
	return 0;
}

static const struct eth_device eth_devices[] =
{
	{
		.if_name = "eth0",
		.gbit_flag = 0,
	}
};

static const struct can_device can_devices[] =
{
	{
		.if_name = "can0",
	},
};

static const struct imu_device imu_devices[] =
{
	{
		.if_name = "imu1",
		.i2c_address = ISM330_I2C_ADDRESS,
	},
};

static const struct component_config acu2_config =
{
	.component_name = "acu2",
	.component_init = &acu2_init,

	.eth_devices = eth_devices,
	.eth_devices_cnt = ARRAY_SIZE(eth_devices),

	.can_devices = can_devices,
	.can_devices_cnt = ARRAY_SIZE(can_devices),

	.imu_devices = imu_devices,
	.imu_devices_cnt = ARRAY_SIZE(can_devices),
};

const struct component_config* component_config_get(void)
{
	return &acu2_config;
}
