#include "lib/lib_ethernet.h"
#include "lib/lib_can.h"
#include "lib/lib_imu.h"
#include "lib/lib_rtkgnss.h"
#include "lib/lib_ethswitch_cli.h"
#include "lib/lib_router.h"

struct component_config
{
	char *component_name;

	int (*component_init)(void);

	const struct eth_device *eth_devices;
	const int eth_devices_cnt;

	const struct can_device *can_devices;
	const int can_devices_cnt;

	const struct imu_device *imu_devices;
	const int imu_devices_cnt;

	const struct rtkgnss_device *rtkgnss_devices;
	const int rtkgnss_devices_cnt;

	const struct eth_switch *eth_switches;
	const int eth_switch_cnt;

	const struct lte_router *lte_routers;
	const int lte_router_cnt;
};
