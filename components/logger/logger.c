#include "bist.h"
#include "lib_ethernet.h"
#include "lib_can.h"

static int logger_init(void)
{
	return 0;
}

static const struct eth_device eth_devices[] =
{
	{
		.if_name = "eth0",
		.gbit_flag = 0,
	}
};

static const struct can_device can_devices[] =
{
	{
		.if_name = "can0",
	},
	{
		.if_name = "can1",
	},
	{
		.if_name = "can2",
	},
};

static const struct component_config logger_config =
{
	.component_name = "logger",
	.component_init = &logger_init,

	.eth_devices = eth_devices,
	.eth_devices_cnt = ARRAY_SIZE(eth_devices),

	.can_devices = can_devices,
	.can_devices_cnt = ARRAY_SIZE(can_devices),
};

const struct component_config* component_config_get(void)
{
	return &logger_config;
}
