#include "bist.h"
#include "lib_rtkgnss.h"
#include "lib_ethernet.h"
#include "lib_ethswitch_cli.h"
#include "lib_router.h"
#include <termios.h>

static int tcu_init(void)
{
	return 0;
}

static const struct eth_device eth_devices[] =
{
	{
		.if_name = "eth0",
		.gbit_flag = 0,
	}
};

static const struct rtkgnss_device rtkgnss_devices[] =
{
	{
		.if_name = "ublox",
		.dev = "/dev/ttyUSB0",
		.tcflags = CS8,
		.baud_rate = B38400,
	}
};

static const struct eth_switch eth_switches[] =
{
	{
		.label = "ethswitch1",
		.ip_address = "192.168.1.200",
		.user = "admin",
		.manufacturer_string = "Hirschmann Automation and Control GmbH",
		.ports = {
			{
				.name = "gbe_1",
				.port_test = "cable-test 1/1",
				.gbit_flag = 1,
				.connected_flag = 0, 
			}, {
				.name = "gbe_2",
				.port_test = "cable-test 1/2",
				.gbit_flag = 1,
				.connected_flag = 1, 
			}, {
				.name = "gbe_3",
				.port_test = "cable-test 1/3",
				.gbit_flag = 1,
				.connected_flag = 0, 
			}, {
				.name = "gbe_4",
				.port_test = "cable-test 1/4",
				.gbit_flag = 1,
				.connected_flag = 1, 
			}, {
				.name = "eth1",
				.port_test = "cable-test 1/5",
				.gbit_flag = 0,
				.connected_flag = 1, 
			}, {
				.name = "eth2",
				.port_test = "cable-test 1/6",
				.gbit_flag = 0,
				.connected_flag = 1, 
			}, {
				.name = "eth3",
				.port_test = "cable-test 1/7",
				.gbit_flag = 0,
				.connected_flag = 1, 
			}, {
				.name = "eth4",
				.port_test = "cable-test 1/8",
				.gbit_flag = 0,
				.connected_flag = 1, 
			}, {
				.name = "eth5",
				.port_test = "cable-test 1/9",
				.gbit_flag = 0,
				.connected_flag = 0, 
			}, {
				.name = "eth6",
				.port_test = "cable-test 1/10",
				.gbit_flag = 0,
				.connected_flag = 1, 
			}, {
				.name = "eth7",
				.port_test = "cable-test 1/11",
				.gbit_flag = 0,
				.connected_flag = 1, 
			}, {
				.name = "eth8",
				.port_test = "cable-test 1/12",
				.gbit_flag = 0,
				.connected_flag = 1, 
			} },
	}
};

static const struct lte_router lte_routers[] =
{
	{
		.label = "lte1",
		.ip_address = "192.168.1.253",
	}
};

static const struct component_config tcu_config =
{
	.component_name = "tcu",
	.component_init = &tcu_init,

	.eth_devices = eth_devices,
	.eth_devices_cnt = ARRAY_SIZE(eth_devices),

	.rtkgnss_devices = rtkgnss_devices,
	.rtkgnss_devices_cnt = ARRAY_SIZE(rtkgnss_devices),

	.eth_switches = eth_switches,
	.eth_switch_cnt = ARRAY_SIZE(eth_switches),

	.lte_routers = lte_routers,
	.lte_router_cnt = ARRAY_SIZE(lte_routers),
};

const struct component_config* component_config_get(void)
{
	return &tcu_config;
}
