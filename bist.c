#include <string.h>
#include <linux/limits.h>
#include <ctype.h>
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdarg.h>
#include <termios.h>
#include <unistd.h>

#include "bist.h"

#include "list_if.h"

#undef BIST_CMD
#include "applets.h"

extern void print_header(void);

const size_t BIST_APPLETS_CNT = ARRAY_SIZE(bist_applets);

/* bist test results vector */
static char *bist_results;

/* bist test return list */
struct bist_cmd_ret
{
	int code;
	char *msg;
	struct list_head list;
};
static LIST_HEAD(ret_list);

static int bist_applet_name_compare(const void *x, const void *y)
{
	const char *name = x;
	const struct bist_applet *applet = y;

	return strcmp(name, applet->name);
}

static struct bist_applet *find_bist_applet_by_name(const char *name)
{
	return bsearch(name, bist_applets, BIST_APPLETS_CNT,
		sizeof(struct bist_applet), bist_applet_name_compare);
}

static void bist_help(void)
{
	int i;
	char buf[80];

	/* help delimiter */
	memset(buf, ' ', sizeof(buf));
	buf[sizeof(buf)-1] = '\0';

	for (i = 0; i < BIST_APPLETS_CNT; i++) {
		printf("%s\n", buf);
		printf("[%s] - %s\n", bist_applets[i].name,
			bist_applets[i].comment);
		printf("%s\n", bist_applets[i].help);
	}
	printf("%s\n", buf);
}

static void bist_menu(void)
{
	int i, indent;

	for (i = 0; i < BIST_APPLETS_CNT; i++) {
		printf("%s %s", bist_applets[i].name, bist_applets[i].comment);

		indent = 80 - (strlen(bist_applets[i].name)
			+ strlen(bist_applets[i].comment) + 1);
		if (indent < 0)
			indent = 0;
		switch (bist_results[i]) {
		case TEST_PASSED:
			printf("%*s", indent, "[PASSED]\n");
			break;
		case TEST_FAILED:
			printf("%*s", indent, "[FAILED]\n");
			break;
		case TEST_RUNNING:
			printf("%*s", indent, "[RUNNING]\n");
			break;
		case TEST_ONGOING:
			printf("%*s", indent, "[ONGOING]\n");
			break;
		default:
			printf("\n");
			break;
		}
	}
	printf("%s %s\n", BIST_MENU, "Menu");
	printf("%s %s\n", BIST_HELP, "Help");
	printf("%s %s\n", BIST_EXIT, "Exit");
}

static int bist_cmd_run(struct bist_applet *cmdtp, int argc, char * argv[])
{
	int result, i;
	int cmd_index = cmdtp - bist_applets;
	struct bist_cmd_ret* cmd_ret;

	/* Clean bist list results */
	list_for_each_entry(cmd_ret, &ret_list, list) {
		free(cmd_ret->msg);
		free(cmd_ret);
	}
	list_del_init(&ret_list);

	bist_results[cmd_index] = TEST_ONGOING;
	result = (cmdtp->main)(argc, argv);
	bist_results[cmd_index] = result;

	/* Print the bist command result */
	printf("[%s,%s]", argv[0],cmdtp->comment);
	printf("[");

	for (i = 1; i < argc; i++)
		printf("%s%s", argv[i], i == argc-1 ? "" : " ");

	printf("]");

	switch (result) {
	case TEST_PASSED:
		printf("[PASSED]");
		break;
	case TEST_FAILED:
		printf("[FAILED]");
		break;
	case TEST_RUNNING:
		printf("[RUNNING]");
		break;
	default:
		break;
	}

	if (result == TEST_FAILED) {
		printf("[");

		list_for_each_entry(cmd_ret, &ret_list, list) {
			printf("%s", cmd_ret->msg);
			if (!list_is_last(&cmd_ret->list, &ret_list))
				printf("; ");
		}
		printf("]");
	}
	printf("\n");

	return result;
}

static int bist_cmd_process(int argc, char * argv[])
{
	struct bist_applet *applet;

	if ((applet = find_bist_applet_by_name(argv[0])) == NULL) {
		printf("Unknown command '%s'\n", argv[0]);
		return -1;
	}

	/* Print help */
	if ((argc > 1) && (strcmp(argv[1], "?") == 0)) {
		printf("[%s] - %s\n", applet->name, applet->comment);
		printf("%s\n", applet->help);
		return 0;
	}

	if (argc > applet->maxargs) {
		printf("Too many args (max. %d)\n", applet->maxargs);
		return -1;
	}

	if (bist_cmd_run(applet, argc, argv) < 0)
		return -1;

	return 0;
}

int bist_exec(const char *fmt, ...)
{
	static char shell_cmd[256];
	va_list args;
	int cmd_status;

	va_start(args, fmt);
	vsnprintf(shell_cmd, sizeof(shell_cmd), fmt, args);
	va_end(args);

	cmd_status = system(shell_cmd);

	if (!WIFEXITED(cmd_status)) {
		printf("%s: '%s' terminated abnormally!\n", __func__, shell_cmd);

		if (WIFSIGNALED(cmd_status))
			printf("%s: was terminated with signal %d\n", __func__, WTERMSIG(cmd_status));

		return -1;
	}

	return WEXITSTATUS(cmd_status);
}

static int bist_parse_line(char *line, char *argv[])
{
	int nargs = 0;
	int quotes = 0;

	while (nargs < ARG_MAX) {
		/* skip any white space */
		while (isblank(*line) || *line == '[' || *line == ']')
			++line;

		if (*line == '"') {
			quotes = 1;
			++line;
		}

		if (*line == '\0') {
			argv[nargs] = NULL;
			return (nargs);
		}

		argv[nargs++] = line;

		if (quotes == 1) {
			/* find end of quotes */
			while (*line && *line != '"')
				++line;
			quotes = 0;
		}
		else {
			/* find end of string */
			while (*line && !isblank(*line) && *line != '[' && *line != ']')
				++line;

			if (*line == '\0') {
				argv[nargs] = NULL;
				return (nargs);
			}
		}

		*line++ = '\0';
	}

	return (nargs);
}

int bist_getc(void)
{
	int c;
	struct termios _termio, termio;

	/* change input terminal settings */
	tcgetattr(STDIN_FILENO, &termio);
	tcgetattr(STDIN_FILENO, &_termio);
	termio.c_lflag &= ~(ECHO | ICANON);
	tcsetattr(STDIN_FILENO, TCSANOW, &termio);

	c = getchar();

	/* restore input terminal settings */
	tcsetattr(STDIN_FILENO, TCSANOW, &_termio);

	return c;
}

int bist_printf(const char *fmt, ...)
{
	int cnt;
	va_list args;

	va_start(args, fmt);
	cnt = vfprintf(stdout, fmt, args);
	va_end(args);

	return cnt;
}

int bist_errmsg(const char *fmt, ...)
{
	int cnt;
	va_list args;
	char buf[4096];

	struct bist_cmd_ret *cmd_ret;

	va_start(args, fmt);
	cnt = vsnprintf(buf, sizeof(buf), fmt, args);
	va_end(args);

	cmd_ret = malloc(sizeof(*cmd_ret));
	cmd_ret->code = TEST_FAILED;
	cmd_ret->msg = strdup(buf);
	list_add_tail(&cmd_ret->list, &ret_list);

	return cnt;
}

int bist_print_buffer(unsigned long addr, void *data, unsigned int width,
	unsigned int count)
{
	/* linebuf as a union causes proper alignment */
	union linebuf
	{
		unsigned int   ui[16/sizeof(unsigned int) + 1];
		unsigned short us[16/sizeof(unsigned short) + 1];
		unsigned char  uc[16/sizeof(unsigned char) + 1];
	} lb;
	int i, linelen = 16 / width;

	while (count)
	{
		bist_printf("%08lx:", addr);

		if (count < linelen)
			linelen = count;

		for (i = 0; i < linelen; i++)
		{
			unsigned int x;
			if (width == 4)
				x = lb.ui[i] = *(unsigned int *)data;
			else if (width == 2)
				x = lb.us[i] = *(unsigned short *)data;
			else
				x = lb.uc[i] = *(unsigned char *)data;
			bist_printf(" %0*x", width * 2, x);
			data += width;
		}

		for (i = 0; i < linelen * width; i++)
		{
			if (!isprint(lb.uc[i]) || lb.uc[i] >= 0x80)
				lb.uc[i] = '.';
		}
		lb.uc[i] = '\0';
		bist_printf("    %s\n", lb.uc);

		/* Update references */
		addr += linelen * width;
		count -= linelen;
	}

	return 0;
}

int main(int argc, char** argv)
{
	char *line;
	int line_argc;
	char *line_argv[ARG_MAX];

	bist_results = malloc(BIST_APPLETS_CNT * sizeof(*bist_results));

	if (bist_results == NULL)
		return EXIT_FAILURE;

	memset(bist_results, 0xff, BIST_APPLETS_CNT * sizeof(*bist_results));

	const struct component_config *component_cfg = component_config_get();

	/* Board specific initializations */
	if (component_cfg->component_init != NULL) {
		(*component_cfg->component_init)();
	}

	print_header();
	bist_menu();

	while (1) {
		line = readline(PROMPT);
		if (!line)
			return EXIT_FAILURE;
		else
			add_history(line);

		line_argc = bist_parse_line(line, line_argv);
		if (line_argc <= 0) {
			free(line);
			continue;
		}

		/* Menu bist */
		if (!strncmp(line_argv[0], BIST_MENU, strlen(BIST_MENU)) || !strncmp(line_argv[0], "?", strlen("?"))) {
			bist_menu();
			free(line);
			continue;
		}
		/* Help bist */
		if (!strncmp(line_argv[0], BIST_HELP, strlen(BIST_HELP))) {
			bist_help();
			free(line);
			continue;
		}
		/* Exit bist */
		if (!strncmp(line_argv[0], BIST_EXIT, strlen(BIST_EXIT)) || !strncmp(line_argv[0], "exit", strlen("exit"))) {
			free(line);
			break;
		}

		bist_cmd_process(line_argc, line_argv);
		free(line);
	}
}
