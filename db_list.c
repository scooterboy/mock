
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


struct node
{
    uint32_t offset;
    uint32_t size;
    uint32_t tag;
    uint8_t *data;
};

static uint32_t calc_new_size(const list_t *list, uint32_t tag, uint32_t buffer_size)
{
    bool done = false;
    struct node *node = NULL;
    uint32_t size_to_drop = 0;
    uint32_t size = 0;

    LOGD("%s", __func__);

    if (list) {
        node = (struct node *)list->buffer;

        while (!done) {
            if (tag == node->tag)
                size_to_drop += node->size;

            if (node->offset <= 0)
                done = true;

            node = (struct node *)&list->buffer[node->offset];
        }

        size = list->size - size_to_drop + buffer_size;
    }
    else
        size = sizeof(list_t) + buffer_size + (2*sizeof(struct node));

    return size;
}

int db_update_list(list_t **listp, uint32_t tag,
                           uint8_t *buffer,
                           uint32_t buffer_size)
{
    struct node *node = NULL;
    struct node *new_node;
    list_t *list = *listp;
    uint32_t new_size = 0;
    uint32_t old_size = 0;
    uint32_t index = 0;
    uint8_t *data;
    bool done = false;
    list_t *new_list;

    LOGD("%s", __func__);

    new_size = calc_new_size(list, tag, buffer_size);

    if (list) {
        old_size = list->size;
        node = (struct node *)list->buffer;
    }

    /* If we get a list of different size, allocate memory for it.
     * Otherwise reuse old list */
    if (new_size != old_size) {
        new_list = (list_t *)malloc(new_size);

        if (!new_list)
            return -ERROR_ALLOC;

        memset(new_list,0,new_size);
        new_list->size = new_size;
    }
    else
        new_list = list;

    new_node = (struct node *)new_list->buffer;

    while (!done && list) {
        if (tag != node->tag) {
            memcpy(new_node,node,node->size + sizeof(struct node));
            index += node->size + sizeof(struct node);
            new_node->offset = index;
            new_node = (struct node *)&new_list->buffer[index];
        }

        node = (struct node *)&list->buffer[node->offset];

        if (node->offset <= 0)
            done = true;
    }

    if (buffer && buffer_size > 0) {
        new_node->offset = index + buffer_size + sizeof(struct node);
        new_node->size = buffer_size;
        new_node->tag = tag;
        data = &new_list->buffer[index+sizeof(struct node)];
        new_node->data = data;

        memcpy(data,buffer,buffer_size);

        new_node = new_node + new_node->offset;
    }

    if (list && (list != new_list)) {
        free(*listp);
    }

    *listp = new_list;

    return 0;
}

int db_get_node(list_t *list, uint32_t tag,
                        uint8_t *buffer,
                        uint32_t *buffer_size)
{
    struct node *node;
    uint8_t *data;
    bool done = false;

    LOGD("%s", __func__);

    if (!list || !buffer)
        return -ERROR_INPUT;

    node = (struct node *)list->buffer;
    data = (uint8_t *)node + sizeof(struct node);

    while (!done) {
        if (tag == node->tag) {
            *buffer_size = node->size;
            memcpy(buffer,data,node->size);
            done = true;
        }
        else {
            node = (struct node *)&list->buffer[node->offset];
            data = (uint8_t *)node + sizeof(struct node);
            done = node->offset <= 0;
        }
    }

    return 0;
}
