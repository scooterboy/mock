#include <stdio.h>
#include "bist.h"
#include "imu.h"
#include <stddef.h> /* needed by Cgos.h */
#include "Cgos.h"
#include <string.h>

static int imu_is_initialized;		/* BSS initializes to zero */
static HCGOS hCgos;
static int imu_verbose = 0;

static int imu_initialize(unsigned long *bus)
{
	unsigned long cnt, dw_unit, dw_type;
	int found = imu_is_initialized;

	if (!imu_is_initialized) {
		/* Install the library */
		unsigned long ver = CgosLibGetVersion();
		if (imu_verbose)
			bist_printf("CGOS version %ld\n", ver);

		if (!CgosLibInitialize()) {
			bist_errmsg("Cannot initialize CGOS library");
			return 1;
		}

		/* Open the Cgos board */
		if (!CgosBoardOpen(0,0,0,&hCgos)) {
			bist_errmsg("Cannot open CGOS board");
			return 1;
		}


		imu_is_initialized = found;
	}

	cnt = CgosI2CCount(hCgos);

	/* Navigate to the correct I2C bus */
	for (dw_unit = 0; dw_unit < cnt; dw_unit++) {
		dw_type = CgosI2CType(hCgos, dw_unit);
		if (dw_type == CGOS_I2C_TYPE_PRIMARY) {
			*bus = dw_unit;
			found = 1;
		}
	}

	if (!found) {
		bist_errmsg("Cannot find IMU on I2C bus");
		return 1;
	}

	return 0;
}

static int imu_test_presence(const struct imu_device *imu_dev)
{
	unsigned char who_am_i;
	unsigned long bus;

	/* Initalize cgos library, scan the I2C bus for the IMU */
	if (imu_initialize(&bus)) {
		return 1;
	}

	/* Read the WHO_AM_I register */
	if (!CgosI2CReadRegister(hCgos,
			bus,
			(unsigned char) (imu_dev->i2c_address | 0x01),
			ISM330_WHO_AM_I,
			&who_am_i)) {

		bist_errmsg("Cannot read WHO_AM_I register");
		return 1;
	}

	/* Verify the WHO_AM_I register */
	if (who_am_i != ISM330_WHOAMI_ID) {
		bist_errmsg("WHO_AM_I reads %02x, expect %02x", who_am_i, ISM330_WHOAMI_ID);
		return 1;
	}

	if (imu_verbose)
		bist_printf("IMU (%lx) reports correct WHO_AM_I value (%02x)\n", bus, who_am_i);

	return 0;
}

/* IMU PRESENCE TEST */
int do_imu_presence(int argc, char *argv[])
{
	int rv;
	const struct component_config *comp = component_config_get();

	imu_verbose = 0;

	if (argc == 2 && !strncmp(argv[1], "v", 1))
		imu_verbose = 1;

	/* Sanity check, should be exactly one IMU configured */
	if (comp->imu_devices_cnt != 1) {
		bist_errmsg("Bad IMU configuration count: %d (expect 1)", comp->imu_devices_cnt);
		return TEST_FAILED;
	}

	rv = imu_test_presence(&comp->imu_devices[0]);

	if (rv) {
		bist_errmsg("%s not present", comp->imu_devices[0].if_name);
		return TEST_FAILED;
	} else {
		bist_printf("%s is present", comp->imu_devices[0].if_name);
	}

	return TEST_PASSED;
}
