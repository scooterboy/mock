#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <poll.h>
#include <fcntl.h>
#include <sys/time.h>
#include "bist.h"

#define MAX_BUFSIZE_IN		1024
#define TIMEOUT_POLL_MS		20000
#define TIMEOUT_READ_SEC	10

static int set_interface_attribs(int fd, int speed, int flags)
{
	struct termios tty;

	memset(&tty, 0, sizeof tty);

	cfsetospeed(&tty, speed);
	cfsetispeed(&tty, speed);

	tty.c_cflag |= CREAD | CLOCAL | CS8;
	tty.c_iflag |= IGNBRK;

	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
		perror("set_interface_attribs: tcsetattr");
		return 1;
	}

	tcflush(fd, TCIOFLUSH);
	usleep(500);
	tcflush(fd, TCIOFLUSH);
	usleep(500);

	fcntl(fd, F_SETFL, 0);

	return 0;
}

static int nmea_read_frame(int s)
{
	struct pollfd fd;
	char *buf = malloc(MAX_BUFSIZE_IN);
	char *p = &buf[0];
	unsigned int begin, end;
	int done = 0;
	int ret;
	unsigned int in_cnt = 0;
	enum states {SYNC, NMEA} state = SYNC;
	struct timeval start_time, now_time, total_time;

	gettimeofday(&start_time, NULL);

	fd.fd = s;
	fd.events= POLLIN;

	while (!done) {
		/* Make sure we don't wait forever */
		gettimeofday(&now_time, NULL);
		timersub(&now_time, &start_time, &total_time);
		if (total_time.tv_sec > TIMEOUT_READ_SEC) {
			bist_errmsg("%s took more than %d seconds, operation timeout", __func__, TIMEOUT_READ_SEC);
			goto err1;
		}

		ret = poll(&fd, 1, TIMEOUT_POLL_MS);
		switch (ret) {
		case -1:
			bist_errmsg("Poll error");
			goto err1;
		case 0:
			bist_errmsg("Polling timeout");
			goto err1;
		default:
			if (p - buf > MAX_BUFSIZE_IN) {
				bist_errmsg("Input buffer overflow");
				goto err1;
			}
			read(s, p, 1);
			in_cnt++;
			switch (state) {
			case SYNC:
				/* NMEA frames begin with $ character */
				if (*p == '$') {
					state++;
					begin = in_cnt;
				}
				break;
			case NMEA:
				/* NMEA frames end with \r\n */
				if (in_cnt > 1 && *p == '\n' && *(p-1) == '\r') {
					done = 1;
					end = in_cnt;
				}

				break;
			}
			p++;
		}
	}
	if (done) {
		bist_dbg("NMEA Frame received:\n");
		for (int i = begin; i < end; i++)
			bist_dbg(" %02x", buf[i]);

		bist_dbg("\n");
	}
err1:
	free(buf);
	return done ? 0 : 1;
}

static int rtkgnss_test_presence(const struct rtkgnss_device *rtkgnss_dev)
{
	int fd;
	int ret = 1;

	/* Open the device */
	fd = open(rtkgnss_dev->dev, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (fd < 0) {
		bist_errmsg("Open %s failed\n", rtkgnss_dev->dev);
		goto err_no_close;
	}

	if (set_interface_attribs(fd, rtkgnss_dev->baud_rate, rtkgnss_dev->tcflags)) {
		bist_errmsg("set_interface_attribs failed\n");
		goto err_close;
	}

	/* See that we can read in an entire NMEA frame */
	if (nmea_read_frame(fd)) {
		bist_errmsg("nmea read frame failed\n");
		goto err_close;
	}

	ret = 0;

err_close:
	close(fd);
err_no_close:
	return ret;
}

/* RTK/GNSS PRESENCE TEST */
int do_rtkgnss_presence(int argc, char *argv[])
{
	int rv;
	const struct component_config *comp = component_config_get();

	if (argc != 1) {
		bist_printf("No arguments expected");
		return TEST_FAILED;
	}

	/* Sanity check, should be exactly one RTK/GNSS configured */
	if (comp->rtkgnss_devices_cnt != 1) {
		bist_errmsg("Bad RTK/GNSS configuration count: %d (expect 1)", comp->rtkgnss_devices_cnt);
		return TEST_FAILED;
	}

	rv = rtkgnss_test_presence(&comp->rtkgnss_devices[0]);

	if (rv) {
		bist_errmsg("%s not present or failed", comp->rtkgnss_devices[0].if_name);
		return TEST_FAILED;
	} else {
		bist_printf("%s is present", comp->rtkgnss_devices[0].if_name);
	}

	return TEST_PASSED;
}
