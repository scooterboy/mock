/*
 * Test the Ethernet switch via the CLI interface
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "bist.h"
#include "lib_ethswitch_cli.h"
#include "switch_config_checksum.h"

#define RESP_SIZE	200
#define CMD_SIZE	200
#define TEMP_NAME_SIZE	40
#define IP_ADDR_SIZE	40

static char g_p[256];
static int switch_verbose = 0;
static char switch_ip[IP_ADDR_SIZE];

static void process_cmd_args(int argc, char *argv[])
{
	if (argc > 1) {
		int argcnt = argc;
		while (--argcnt > 0) {
			if (argv[argcnt][0] == 'v')
				switch_verbose = 1;
			else
				snprintf(switch_ip, IP_ADDR_SIZE, "%s", argv[argcnt]);
		}
	}
}

static void dump_line(char *s)
{
	if (switch_verbose)
		bist_printf("%s", s);
	else
		bist_dbg("%s", s);
}

static int get_p()
{
	FILE *fp = fopen("/opt/.db", "rb");
	unsigned char p;
	int i = 0;
	int x;

	if (!fp) {
		bist_errmsg("Cannot open db file");
		return 1;
	}

	do {
		x = fread(&p, sizeof(p), 1, fp);
		if (x == 1) {
			g_p[i++] = (char) p + 0x21;
		}
	} while (x == 1);

	g_p[i] = 0;

	return 0;
}

static int cli_session(const char *cmds[],
		const char *result,
		const struct component_config *comp)
{
	char cmd[CMD_SIZE];
	const char ssh_options[] = "-o StrictHostKeyChecking=no -o LogLevel=QUIET -tt";
	int i = 0;

	if (get_p())
		return 1;

	snprintf(cmd, CMD_SIZE, "sshpass -p \"%s\" ssh %s %s@%s > %s",
		g_p,
		ssh_options,
		comp->eth_switches[0].user,
		switch_ip,
		result);

	FILE *fp = popen(cmd,"w");

	if (!fp) {
		bist_errmsg("Cannot execute ssh command");
		return 1;
	}

	while (cmds[i] != NULL)
		fprintf(fp, "%s\n", cmds[i++]);

	fprintf(fp, "logout\n");
	fprintf(fp, "y\n");	// Answer prompt to really exit
	fprintf(fp, "n\n");	// In case we get Save Unsaved Changes prompt

	if (pclose(fp) < 0) {
		bist_errmsg("Failed to close session");
		return 1;
	}

	return 0;
}

/*
 * Test the presence of a CLI interface from the TCU to the Ethernet Switch
 */
int do_eth_switch_cli(int argc, char *argv[])
{
	const char *cmds[] = {NULL};
	char temp_file_name[TEMP_NAME_SIZE];
	char resp[RESP_SIZE];
	int rv = TEST_FAILED;
	const struct component_config *comp = component_config_get();

	memset(temp_file_name, 0, TEMP_NAME_SIZE);

	if (!comp) {
		bist_errmsg("Could not get component struct");
		return TEST_FAILED;
	}

	snprintf(switch_ip, IP_ADDR_SIZE, "%s", comp->eth_switches[0].ip_address);
	switch_verbose = 0;

	process_cmd_args(argc, argv);

	snprintf(temp_file_name, TEMP_NAME_SIZE, "/tmp/presence-XXXXXX");
	int temp = mkstemp(temp_file_name);

	if (temp < 0) {
		bist_errmsg("Cannot open %s\n", temp_file_name);
		return TEST_FAILED;
	}

	FILE *fp = fdopen(temp, "r");

	if (!fp) {
		bist_errmsg("Cannot open %s for reading", temp_file_name);
		unlink(temp_file_name);
		return TEST_FAILED;
	}

	if (cli_session(cmds, temp_file_name, comp))
		goto cleanup;

	while (fgets(resp, RESP_SIZE, fp) != NULL) {
		dump_line(resp);
		if (strstr(resp, comp->eth_switches[0].manufacturer_string))
			rv = TEST_PASSED;
	}

cleanup:
	unlink(temp_file_name);
	fclose(fp);

	return rv;
}

/*
 * Test that the switch is running the Husqvarna configuration,
 * verify the checksum of the Husqvarna configuration,
 * and make sure the running configuration is synced with the
 * Husqvarna configuration.
 */
int do_eth_switch_config_check(int argc, char *argv[])
{
	const char *cmds[] = {"enable",
			"configure",
			"config fingerprint verify nvm profile husqvarna-config " CONFIG_CHECKSUM,
			"show config status",
			"show config profiles nvm",
			NULL};
	char temp_file_name[TEMP_NAME_SIZE];
	char resp[RESP_SIZE];
	const struct component_config *comp = component_config_get();
	int match_flag = 0;
	int sync_flag = 0;
	int active_flag = 0;
	int next_line_is_active = 0;

	memset(temp_file_name, 0, TEMP_NAME_SIZE);

	if (!comp) {
		bist_errmsg("Could not get component struct");
		return TEST_FAILED;
	}

	snprintf(switch_ip, IP_ADDR_SIZE, "%s", comp->eth_switches[0].ip_address);
	switch_verbose = 0;

	process_cmd_args(argc, argv);

	snprintf(temp_file_name, TEMP_NAME_SIZE, "/tmp/config-XXXXXX");
	int temp = mkstemp(temp_file_name);

	if (temp < 0) {
		bist_errmsg("Cannot open %s", temp_file_name);
		return TEST_FAILED;
	}

	FILE *fp = fdopen(temp, "r");

	if (!fp) {
		bist_errmsg("Cannot open %s for reading", temp_file_name);
		unlink(temp_file_name);
		return TEST_FAILED;
	}

	if (cli_session(cmds, temp_file_name, comp))
		goto cleanup2;

	while (fgets(resp, RESP_SIZE, fp) != NULL) {
		dump_line(resp);
		if (strstr(resp, "Profile fingerprint matches !"))
			match_flag = 1;

		if (strstr(resp, "running-config to NVM.......................ok"))
			sync_flag = 1;

		if (next_line_is_active) {
			next_line_is_active = 0;
			if (strstr(resp, "[x]"))
				active_flag = 1;
		}

		if (strstr(resp, "husqvarna-config"))
			next_line_is_active = 1;

	}

cleanup2:
	unlink(temp_file_name);
	fclose(fp);

	if (!match_flag) {
		bist_errmsg("Profile fingerprint mismatch");
		return TEST_FAILED;
	}

	if (!sync_flag) {
		bist_errmsg("Running profile not synced");
		return TEST_FAILED;
	}

	if (!active_flag) {
		bist_errmsg("Husqvarna profile is not active");
		return TEST_FAILED;
	}

	return TEST_PASSED;
}

static int phy_check(const struct switch_port *e,
			int port,
			int up,
			int down,
			int open,
			int short_circuit)
{
	int wires_reported = up + down;
	char report[100];

	if (open || short_circuit)
		bist_printf("Warning on port %s (1/%d): %d short circuits, %d breaks in cable\n", e->name, port, open, short_circuit);

	snprintf(report, 100, "Report on port %s (1/%d): %d up, %d down\n", e->name, port, up, down);
	if (switch_verbose)
		bist_printf("%s", report);
	else
		bist_dbg("%s", report);

	/* Cannot have a port with a mixed count */
	if (up && down) {
		bist_errmsg("Port %s has %d up, %d down", e->name, up, down);
		return 1;
	}

	/* Check for link speed consistency */
	if (e->gbit_flag) {
		if (wires_reported != 4) {
			bist_errmsg("Gbit port %s wires %d (expect 4)", e->name, wires_reported);
			return 1;
		}
	} else if (wires_reported != 2) {
		bist_errmsg("100Mb port %s wires %d (expect 2)", e->name, wires_reported);
		return 1;
	}

	/* Check if port expected to be up is down */
	if (e->connected_flag && !up) {
		bist_errmsg("Port %s is down, expected to be up", e->name);
		return 1;
	}

	/* Check if port expected to be down is up */
	if (!e->connected_flag && !down) {
		bist_errmsg("Port %s is up, expected to be down", e->name);
		return 1;
	}
	return 0;
}

/*
 * Test that the switch connections are correct for the Husqvarna
 * product. Use the CLI command "test cable" in the Ethernet switch.
 */
int do_eth_switch_cable_test(int argc, char *argv[])
{
	char temp_file_name[TEMP_NAME_SIZE];
	char resp[RESP_SIZE];
	int rv = TEST_FAILED;
	int port = 0;
	int cable_up = 0;
	int cable_down = 0;
	int short_circuit = 0;
	int open = 0;
	const struct component_config *comp = component_config_get();
	const char *cmds[] = {"enable",
			comp->eth_switches[0].ports[0].port_test,
			comp->eth_switches[0].ports[1].port_test,
			comp->eth_switches[0].ports[2].port_test,
			comp->eth_switches[0].ports[3].port_test,
			comp->eth_switches[0].ports[4].port_test,
			comp->eth_switches[0].ports[5].port_test,
			comp->eth_switches[0].ports[6].port_test,
			comp->eth_switches[0].ports[7].port_test,
			comp->eth_switches[0].ports[8].port_test,
			comp->eth_switches[0].ports[9].port_test,
			comp->eth_switches[0].ports[10].port_test,
			comp->eth_switches[0].ports[11].port_test,
			NULL};

	memset(temp_file_name, 0, TEMP_NAME_SIZE);

	if (!comp) {
		bist_errmsg("Could not get component struct");
		return TEST_FAILED;
	}

	snprintf(switch_ip, IP_ADDR_SIZE, "%s", comp->eth_switches[0].ip_address);
	switch_verbose = 0;

	process_cmd_args(argc, argv);

	snprintf(temp_file_name, TEMP_NAME_SIZE, "/tmp/phy-XXXXXX");
	int temp = mkstemp(temp_file_name);

	if (temp < 0) {
		bist_errmsg("Cannot open %s", temp_file_name);
		return TEST_FAILED;
	}

	FILE *fp = fdopen(temp, "r");

	if (!fp) {
		bist_errmsg("Cannot open %s for reading", temp_file_name);
		unlink(temp_file_name);
		return TEST_FAILED;
	}


	if (cli_session(cmds, temp_file_name, comp))
		goto cleanup2;

	while (fgets(resp, RESP_SIZE, fp) != NULL) {
		dump_line(resp);
		if (port < ETH_SWITCH_NUM_PORTS &&
			(strstr(resp, comp->eth_switches[0].ports[port].port_test))) {
				if (cable_up || cable_down) {
					/* New test, report on last one */
					int ret = phy_check(&comp->eth_switches[0].ports[port-1],
							port,
							cable_up,
							cable_down,
							open,
							short_circuit);
					if (ret)
						goto cleanup2;
				}
				cable_up = cable_down = open = short_circuit = 0;
				port++;
		} else if (strstr(resp, "unknown")) {
			/* Untested cable pairs */
			cable_down++;
		} else if (strstr(resp, "open")) {
			/* There is a break in the cable */
			cable_down++;
			open++;
		} else if (strstr(resp, "normal")) {
			/* The cable is functioning properly */
			cable_up++;
		} else if (strstr(resp, "short")) {
			/* Wires in the cable are touching, short circuit */
			cable_down++;
			short_circuit++;
		}
	}

	rv = TEST_PASSED;

cleanup2:
	unlink(temp_file_name);
	fclose(fp);

	return rv;
}
