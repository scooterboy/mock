/*
 * LTE router is accessed via SNMP. Must run:
 *
 * export MIBS="ACKSYS-MIB"
 *
 * before using these tests.
 */

#include "bist.h"
#include <string.h>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

#undef DEMO_USE_SNMP_VERSION_3		// TODO turn this on, enable security

#ifdef DEMO_USE_SNMP_VERSION_3
const char *v3_passphrase = "<password_here>";
#endif

#define OID_IPADDR ".iso.org.dod.internet.private.enterprises.acksys.networkConfiguration.tcpip.configIpSubnetTable.configIpSubnetEntry.configIpSubnetIPv4Addr.\"lan\""
#define OID_WIFIENABLE ".iso.org.dod.internet.private.enterprises.acksys.networkConfiguration.netphy.configPhyWifiTable.configPhyWifiEntry.configPhyWifiEnable.\"radio0\""

#define IP_ADDR_SIZE	40

static int snmp_verbose = 0;
static char lte_ip[IP_ADDR_SIZE];

static void init_process_cmd_args(int argc, char *argv[])
{
	setenv("MIBS", "ACKSYS-MIB", 1);
	if (argc > 1) {
		int argcnt = argc;
		while (--argcnt > 0) {
			if (argv[argcnt][0] == 'v')
				snmp_verbose = 1;
			else
				snprintf(lte_ip, IP_ADDR_SIZE, "%s", argv[argcnt]);
		}
	}
}

static void dump_line(char *s)
{
	if (snmp_verbose)
		bist_printf("%s", s);
	else
		bist_dbg("%s", s);
}

static int lte_snmp_read(char *result, const char *my_oid)
{
	netsnmp_session session, *ss;
	netsnmp_pdu *pdu;
	netsnmp_pdu *response = NULL;
	oid anOID[MAX_OID_LEN];
	size_t anOID_len;
	netsnmp_variable_list *vars;
	int status;

	/*
	 * Initialize the SNMP library
	 */
	init_snmp("ltesnmptest");

	/*
	 * Initialize a "session" that defines who we're going to talk to
	 */
	snmp_sess_init(&session);
	session.peername = strdup(lte_ip);

	/* set up the authentication parameters for talking to the server */

#ifdef DEMO_USE_SNMP_VERSION_3

	/* set the SNMP version number */
	session.version=SNMP_VERSION_3;

	/* set the SNMPv3 user name */
	session.securityName = strdup("MD5User");
	session.securityNameLen = strlen(session.securityName);

	/* set the security level to authenticated, but not encrypted */
	session.securityLevel = SNMP_SEC_LEVEL_AUTHNOPRIV;

	/* set the authentication method to MD5 */
	session.securityAuthProto = usmHMACMD5AuthProtocol;
	session.securityAuthProtoLen = sizeof(usmHMACMD5AuthProtocol)/sizeof(oid);
	session.securityAuthKeyLen = USM_AUTH_KU_LEN;

	/*
	 * set the authentication key to a MD5 hashed version of our
	 * passphrase "The Net-SNMP Demo Password" (which must be at least 8
	 * characters long)
	 */
	if (generate_Ku(session.securityAuthProto,
			session.securityAuthProtoLen,
			(u_char *) v3_passphrase, strlen(v3_passphrase),
			session.securityAuthKey,
			&session.securityAuthKeyLen) != SNMPERR_SUCCESS) {

		snmp_perror(argv[0]);
		snmp_log(LOG_ERR,
			"Error generating Ku from authentication pass phrase. \n");
		bist_errmsg("Cannot generate auth key");
		return 1;
	}

#else /* SNMPv1 */

	session.version = SNMP_VERSION_1;

	/* set the SNMPv1 community name used for authentication */
	session.community = (unsigned char *) "public";
	session.community_len = strlen((char *) session.community);

#endif /* SNMPv1 */

	SOCK_STARTUP;
	ss = snmp_open(&session);

	if (!ss) {
		snmp_sess_perror("ack", &session);
		SOCK_CLEANUP;
		bist_errmsg("Session open error");
		return 1;
	}

	/* Create PDU for our request */
	pdu = snmp_pdu_create(SNMP_MSG_GET);
	anOID_len = MAX_OID_LEN;
	if (!snmp_parse_oid(my_oid, anOID, &anOID_len)) {
		snmp_perror(my_oid);
		SOCK_CLEANUP;
		snmp_close(ss);
		bist_errmsg("Parse OID error");
		return 1;
	}

	snmp_add_null_var(pdu, anOID, anOID_len);

	/* Send out the request */
	status = snmp_synch_response(ss, pdu, &response);

	/* Process the response */
	if (status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR) {
		for (vars = response->variables; vars; vars = vars->next_variable) {
			snprint_variable(result, 200, vars->name, vars->name_length, vars);
			dump_line(result);
		}
	} else {
		if (status == STAT_SUCCESS)
			bist_errmsg("Error in packet %s\n",
					snmp_errstring(response->errstat));
		else if (status == STAT_TIMEOUT)
			bist_errmsg("Timeout: No response from %s.\n",
					session.peername);
		else {
			snmp_sess_perror("snmpdemoapp", ss);
			bist_errmsg("Request error");
		}
		SOCK_CLEANUP;
		snmp_close(ss);
		if (response)
			snmp_free_pdu(response);

		return 1;
	}

	if (response)
		snmp_free_pdu(response);

	snmp_close(ss);
	SOCK_CLEANUP;
	return 0;
}

/*
 * Test the SNMP interface to the LTE router
 */
int do_lte_snmp_xfer(int argc, char *argv[])
{
	const struct component_config *comp = component_config_get();
	char buf[200];

	if (!comp) {
		bist_errmsg("Could not get component struct");
		return TEST_FAILED;
	}

	snmp_verbose = 0;
	snprintf(lte_ip, IP_ADDR_SIZE, "%s", comp->lte_routers[0].ip_address);
	init_process_cmd_args(argc, argv);

	if (lte_snmp_read(buf, OID_IPADDR))
		return TEST_FAILED;

	return TEST_PASSED;
}

/*
 * Check the configuration of the router
 *
 * Currently checks only that wifi is disabled. TODO add to this.
 */
int do_lte_config_check(int argc, char *argv[])
{
	const struct component_config *comp = component_config_get();
	char buf[200];

	if (!comp) {
		bist_errmsg("Could not get component struct");
		return TEST_FAILED;
	}

	snmp_verbose = 0;
	snprintf(lte_ip, IP_ADDR_SIZE, "%s", comp->lte_routers[0].ip_address);
	init_process_cmd_args(argc, argv);

	if (lte_snmp_read(buf, OID_WIFIENABLE))
		return TEST_FAILED;

	if (!strcmp(buf, "disable(1)")) {
		bist_errmsg("Wifi is enabled, should be disabled");
		return TEST_FAILED;
	}

	return TEST_PASSED;
}
