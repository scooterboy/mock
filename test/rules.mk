d = test

INCLUDES += -I$(d)

SRCS-$(CONFIG_BIST_PING) += $(d)/eth.c
SRCS-$(CONFIG_BIST_SENSOR_INFO) += $(d)/sensors.c
SRCS-$(CONFIG_BIST_IMU) += $(d)/imu.c
SRCS-$(CONFIG_BIST_RTKGNSS) += $(d)/rtkgnss.c
SRCS-$(CONFIG_BIST_SWITCH_CLI) += $(d)/eth_switch_cli.c
SRCS-$(CONFIG_BIST_LTE) += $(d)/lte_snmp.c
SRCS-$(CONFIG_BIST_CAN) += $(d)/can_scan.c

CFLAGS += $(shell cat components/current/cmd.cfg | grep "CONFIG_BIST.*=y" | grep -v \" | sed -e "s/^CONFIG_BIST/-DBIST/g")

