#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "bist.h"

#define TEMP_NAME_SIZE	40
#define RESP_SIZE	200

int eth_verbose = 0;

/* PING TEST */
int do_ping(int argc, char *argv[])
{
	int rv;
	char* ip = argv[1];

	if (argc != 2) {
		bist_printf("Missing arguments");
		return TEST_FAILED;
	}

	rv = bist_exec("ping %s -c 2", ip);

	if (rv) {
		bist_printf("Cannot ping %s", ip);
		return TEST_FAILED;
	}
	else
		printf("%s is alive", ip);

	return TEST_PASSED;
}

static int check_phy(const struct eth_device *dev)
{
	char temp_file_name[TEMP_NAME_SIZE];
	char resp[RESP_SIZE];
	int gbit = 0;
	int link_up = 0;
	int rv = 0;
	static char *ethtool = "/sbin/ethtool";

	memset(temp_file_name, 0, TEMP_NAME_SIZE);

	snprintf(temp_file_name, TEMP_NAME_SIZE, "/tmp/phy-XXXXXX");
	int temp = mkstemp(temp_file_name);

	if (temp < 0) {
		bist_errmsg("Cannot open %s\n", temp_file_name);
		return 1;
	}

	FILE *fp = fdopen(temp, "r");

	if (!fp) {
		bist_errmsg("Cannot open %s for reading", temp_file_name);
		unlink(temp_file_name);
		return 1;
	}

	if (access(ethtool, X_OK) == -1) {
		bist_errmsg("%s not installed", ethtool);
		return 1;
	}

	bist_exec("%s %s 2> /dev/null > %s", ethtool, dev->if_name, temp_file_name);

	while (fgets(resp, RESP_SIZE, fp) != NULL) {
		if (eth_verbose)
			bist_printf("%s", resp);
		else
			bist_dbg("%s", resp);

		if (strstr(resp, "Speed: 1000Mb/s"))
			gbit = 1;

		if (strstr(resp, "Link detected: yes"))
			link_up = 1;
	}

	if (!link_up) {
		rv = 1;
		bist_errmsg("Link %s down", dev->if_name);
	}

	if (dev->gbit_flag && !gbit) {
		rv = 1;
		bist_errmsg("Link %s is not a Gbit port", dev->if_name);
	}

	if (!dev->gbit_flag && gbit) {
		rv = 1;
		bist_errmsg("Link %s is not a 100Mb port", dev->if_name);
	}

	unlink(temp_file_name);
	fclose(fp);
	return rv;
}

int do_eth_port(int argc, char *argv[])
{
	int rv, result = TEST_PASSED;
	int i;
	const struct component_config *comp = component_config_get();

	if (!comp) {
		bist_errmsg("Could not get component struct");
		return TEST_FAILED;
	}

	eth_verbose = 0;

	if (argc == 2 && !strncmp(argv[1], "v", 1))
		eth_verbose = 1;

	for (i=0; i<comp->eth_devices_cnt; i++) {
		rv = check_phy(&comp->eth_devices[i]);
		if (rv)
			result = TEST_FAILED;
	}

	return result;
}

