#include <stdio.h>
#include <string.h>
#include "bist.h"

#define CMD_SIZE	100
#define TEMP_NAME_SIZE	80
#define RESP_SIZE	100

/* Scan the CAN bus */
int do_can_scan(int argc, char *argv[])
{
	const struct component_config *comp = component_config_get();

	if (argc != 1) {
		bist_printf("No arguments expected");
		return TEST_FAILED;
	}

	if (!comp) {
		bist_errmsg("Could not get component struct");
		return TEST_FAILED;
	}

	for (int i = 0; i < comp->can_devices_cnt; i++) {
		if (bist_exec("/opt/bin/canopen-scan.sh %s", comp->can_devices[i].if_name)) {
			bist_errmsg("CAN scan failure %s", comp->can_devices[i].if_name);
			return TEST_FAILED;
		}
	}

	return TEST_PASSED;
}
