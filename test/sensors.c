#include "bist.h"

int do_print_sensor_info(int argc, char *argv[])
{
	int rv;

	rv = bist_exec("sensors");

	if (rv) {
		bist_errmsg("Could not print sensor information");
		return TEST_FAILED;
	}

	return TEST_PASSED;
}
