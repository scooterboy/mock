#if defined(PROTOTYPES)
#define BIST_CMD(a,b,c,d,e) extern int c(int argc, char **argv);
#else
struct bist_applet {
	const char *name;
	int maxargs;
	int (*main)(int argc, char** argv);
	const char *comment;
	const char *help;
};
#define BIST_CMD(a,b,c,d,e) {#a,b,c,d,e},
const struct bist_applet bist_applets[] = {
#endif

#ifdef BIST_PING
	BIST_CMD(01, 2, do_ping, "Net ping [ip]",
		"[01][address]\n"
		" - send ICMP ECHO_REQUEST to network host."
		)
#endif

#ifdef BIST_ETH_STATUS
		BIST_CMD(02, 2, do_eth_port, "Ethernet port test [v]",
			" - Test the Ethernet port cabling")
#endif

#ifdef BIST_SWITCH_CLI
		BIST_CMD(03, 3, do_eth_switch_cli, "Eth switch CLI test [ip] [v]",
			" - Test for the presence of an Ethernet switch CLI interface")
		BIST_CMD(04, 3, do_eth_switch_config_check, "Eth switch config check [ip] [v]",
			" - Verify the Ethernet switch is running the correct configuration")
		BIST_CMD(05, 3, do_eth_switch_cable_test, "Eth switch cable test [ip] [v]",
			" - Verify the Ethernet switch cabling with CLI command")
#endif

#ifdef BIST_SENSOR_INFO
		BIST_CMD(10, 1, do_print_sensor_info, "Print sensor information",
			" - show the current readings of sensor chips.")
#endif

#ifdef BIST_IMU
		BIST_CMD(15, 2, do_imu_presence, "IMU presence test [v]",
			" - Test for the presence of IMU sensor\n"
			"[03]\n"
			" - Test for the presence of IMU sensor")
#endif

#ifdef BIST_RTKGNSS
		BIST_CMD(20, 1, do_rtkgnss_presence, "RTK/GNSS presence test",
			" - Test for the presence of RTK/GNSS hardware")
#endif

#ifdef BIST_LTE
		BIST_CMD(25, 3, do_lte_snmp_xfer, "LTE router SNMP test [ip] [v]",
			" - Test SNMP communication to the LTE router")
		BIST_CMD(26, 3, do_lte_config_check, "LTE router config check [ip] [v]",
			" - Verify the LTE router configuration")
#endif

#ifdef BIST_CAN
		BIST_CMD(30, 1, do_can_scan, "Scan the CAN bus",
			" - Scan the CAN bus, verify CAN communication to other nodes")
#endif

#if !defined(PROTOTYPES)
};
#endif
