#!/bin/sh

GIT_STAT=$(printf '%s' $(git diff --stat))
GIT_REV=$(printf '%s' $(git rev-parse --short HEAD))
if [ -n "$GIT_STAT" ]; then
	GIT_REV="${GIT_REV}-dirty"
fi

cat <<EOF
#include <stdio.h>
#include "bist.h"

char *_git_rev = "$GIT_REV";

void print_header(void)
{
	const struct component_config *component_cfg = component_config_get();

	printf("\\n Built in Self Test: %s (git hash %s)\\n",component_cfg->component_name, _git_rev);
}
EOF